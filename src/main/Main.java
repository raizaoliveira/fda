package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.errors.AmbiguousObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.errors.RevisionSyntaxException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;

import analysis.core.AstLogger;
import analysis.core.Project;
import analysis.core.ResultsLogger;
import cdt.handlers.SampleHandler;
import finegrained.Reports;
import metrics.Metrics;



public class Main {
	
	//the path for all files
	public static String PATH = "";
	
	static List<String> projectPaths = new ArrayList<String>();
	
	//this array is set later
	static String[] versionsToAnalyseOfEachProject = null;
	
	//paths define from info.txt
	static String[] pathsToResults = null;
	
	//parameters
	static String[] parameters = null;
	
	//download path where all the git project will be downloaded
	public static String downloadPath = "";
	
	//git refer
	static Git git;
	
	//all the commit ids to analyse
	public static List<String> commitsIdToAnalyse = new ArrayList<String>();
	
	//keep record of the last commit to compare
	static String lastCommitAnalysed = "";
	
	//keep record of the last project been analyzing
	public static String currentProject = "";
	
	//keep record to clean ast list
	public static List<String> filesToDeleteFromAnalysisFolder = new ArrayList<String>();
	
	//keep record of the current tag analysing
	public static List<String> currentTag;
	
	//keep record of the current commit id
	public static String currentCommit;
	
	//branch to analyze
	public static String branchToAnalyze;
	
	//keeping track of project
	public static Project project;
	
	//decide to not analyse the first version
	public static boolean analyseThisTime = true;
	
	//the number of past anaylsis
	public static int numberOfAnalysisOcurred = 0;
	
	//decide if Analyser.java will run this time
	public static boolean noChangesInCFiles = false;
	
	public static Date commitDate;
	
	public static List<String> allCommitsThisAnalysis;
	
	public static void start(String runTimeWorkspacePath) throws IOException{
		long startTime = System.nanoTime();

		
		PATH = runTimeWorkspacePath;
		Metrics.path = PATH;
		
		setInfos();
		
		//registering the number of times
		int i = 0;

		//checking if paths are github or directory
		for(String projectPath : projectPaths){
			
			//resetting variables
			currentCommit = "";
			lastCommitAnalysed = "";
			Metrics.index = 1;
			Reports.rindex = 1;
			exportFunctionCalls.ExportNumberOfCalls.i = 1;
			commitsIdToAnalyse = new ArrayList<String>();
			filesToDeleteFromAnalysisFolder = new ArrayList<String>();
			
			noChangesInCFiles = false;
			
			//0 = download, 1 = analyseVersions, 2 = analyseCommits, 3 = noStubs, 4 = allR
			boolean settingsFromParameters[] = settingsFromParameters();
			
			//if download is enable
			downloadPath = PATH + pathsToResults[i] + "\\";
			currentProject = pathsToResults[i];
			SampleHandler.PROJECT = currentProject;
			System.out.println(downloadPath);
			if(settingsFromParameters[0]){
				new File(downloadPath).mkdirs();
				downloadProject(projectPath);
			}
			
			else{
				git = Git.open(new File(projectPath));
			}
			
			defineAllCommitsId(settingsFromParameters,versionsToAnalyseOfEachProject[i]);
			
			//set AstLogger
			AstLogger.setWriter();
			//set ResultsLogger
			ResultsLogger.setWriter();
			
			Starter analyser = new Starter(downloadPath, settingsFromParameters[3]);
			
			int numberOfAnalysis = 1; //
			//analyse only versions
			
			int indexOfPastAnalysis = getIndexOfPastAnalysis();
			
			//writing all the commitsId to analyze
			FileWriter writer = new FileWriter(new File(downloadPath + "results/" + "0_allCommitsId.txt"));
			for(String commit : commitsIdToAnalyse){
				writer.write(commit + "\n");
			}
			writer.close();
			
			ResultsLogger.write("Number of commitsId: " + commitsIdToAnalyse.size());
			if(indexOfPastAnalysis != -1){
				deleteAllFromAnalysisFolder();
				numberOfAnalysisOcurred = indexOfPastAnalysis;
				analyseThisTime = false;
	    		allCommitsThisAnalysis = new ArrayList<String>(commitsIdToAnalyse.size());
	    		allCommitsThisAnalysis.addAll(commitsIdToAnalyse);
	    		System.out.println(indexOfPastAnalysis+"   "+commitsIdToAnalyse.size());
				//commitsIdToAnalyse = commitsIdToAnalyse.subList(indexOfPastAnalysis-1, commitsIdToAnalyse.size());
			}
			else {
				allCommitsThisAnalysis = new ArrayList<String>(commitsIdToAnalyse.size());
	    		allCommitsThisAnalysis.addAll(commitsIdToAnalyse);
			}
			System.out.println("Selected commitsId size: " + commitsIdToAnalyse.size());
			
			//writing all the commitsId to analyze after the selection
			writer = new FileWriter(new File(downloadPath + "results/" + "1_allCommitsAfterSelection.txt"));
			for(String commit : commitsIdToAnalyse){
				writer.write(commit + "\n");
			}
			writer.close();
			
			for(String commit : commitsIdToAnalyse){
				
				currentCommit = commit;
				//writing to results/errorPath/errorTxt.txt
				AstLogger.write("\nAnalysis number: " + numberOfAnalysis);
				numberOfAnalysis++;
				try{
					//this file needs to be deleted for the next analysis
					Files.delete(new File(SampleHandler.RUNTIME_WORKSPACE_PATH + SampleHandler.PROJECT + "/temp2.c").toPath());
				}
				catch(Exception e){
					//in case of the file doesnt exist
				}
		
				changeToVersion(commit);
				System.out.println(commit);
				
				List<String> modifiedFilesArrayList = null;
						
				modifiedFilesArrayList = settingUp(commit);
				String modifiedFiles[] = null;
				if(modifiedFilesArrayList != null){
					modifiedFiles = new String[modifiedFilesArrayList.size()];
					modifiedFiles = modifiedFilesArrayList.toArray(modifiedFiles);
				}
				
				//writing to results/specs.txt
				ResultsLogger.write("Commit " + commit);
				
				//this method will create the stubs file, generate all ASTs and create results/dependencies.txt
				project = analyser.start(modifiedFiles);

			}
			
			git.getRepository().close();	
			i++;
		}
		
		long elapsedTime = System.nanoTime() - startTime;
		System.out.println("Analysis ended in " + TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS) + " seconds.");
	}
	
	@SuppressWarnings("resource")
	public static void defineAllCommitsId(boolean[] settings, String versions){
		String versionsDefined[] = versions.split(",");
		commitsIdToAnalyse = new ArrayList<String>();
		currentTag = new ArrayList<String>(versionsDefined.length);
		try {
			git.checkout().setName(branchToAnalyze).call();
		} catch (GitAPIException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(settings[1]){
			//get all the commits id from versions
			try {
				List<Ref> tagRefs = git.tagList().call();
				List<String> allCommitsId = new ArrayList<String>(versionsDefined.length);
				List<String> allVersionNames = new ArrayList<String>(allCommitsId.size());
		    	for(Ref ref : tagRefs) {
		            String versionNumber = ref.getName().split("/")[2];
		            allVersionNames.add(versionNumber);
		            
		            String commitId = ref.getObjectId().getName();
		            allCommitsId.add(commitId);
		        }
		    	for(String version : versionsDefined){
		    		for(int i = 0 ; i < allVersionNames.size() ; i++) {
		    			if(version.equals(allVersionNames.get(i))) {
		    				commitsIdToAnalyse.add(allCommitsId.get(i));
		    				break;
		    			}
			    	}
	            }
				
			} catch (GitAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(settings[2]){
			//get all the commits id between the versions

			if(versionsDefined[0].equals("0")){
				try {
	                Iterable<RevCommit> logs = git.log().call();
	                int count = 0;
	                for (RevCommit rev : logs) {
	                	commitsIdToAnalyse.add(rev.getName());
	                	
//	                    System.out.println("Commit: " + rev + ", name: " + rev.getName() + ", id: " + rev.getId().getName());
	                    count++;
	                    
	                }
	                System.out.println("Number of commits: " + count);
				} catch (GitAPIException e) {
					e.printStackTrace();
				}
				Collections.reverse(commitsIdToAnalyse);
				return;
			}
			try {
				List<Ref> tagRefs = git.tagList().call();
				List<String> commitsToAdd = new ArrayList<String>();
				for(Ref ref : tagRefs) {
		            String versionNumber = ref.getName().split("/")[2];
		            String commitId = ref.getObjectId().getName();
		            
		            for(String version : versionsDefined){
		            	if(versionNumber.contains(version)){
		            		commitsToAdd.add(commitId);
			            }
		            }
		        }
				git.reset().call();

	    		Iterable<RevCommit> logs;
	    		
	    		//next 4 lines are about to transform the tag id into the commit id from the moment above the creation of tag
	    		ObjectId tagIdFrom = git.getRepository().resolve(commitsToAdd.get(0));
	    		ObjectId tagIdTo = git.getRepository().resolve(commitsToAdd.get(1));
	    		RevCommit commitIdFrom = git.getRepository().parseCommit(tagIdFrom);
	    		RevCommit commitIdTo = git.getRepository().parseCommit(tagIdTo);

	    		logs = git.log().addRange(commitIdFrom.getId(),commitIdTo.getId()).call();
				
	 
	    		for(RevCommit rev : logs){
					commitsIdToAnalyse.add(rev.getName());
	    		}
	    		
	    		Collections.reverse(commitsIdToAnalyse);
	    		
			} catch (GitAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RevisionSyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MissingObjectException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IncorrectObjectTypeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (AmbiguousObjectException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(settings[4]){
			try {
				List<Ref> tagRefs = git.tagList().call();
				for(Ref ref : tagRefs) {
					
					String versionNumber = ref.getName().split("/")[2];
					currentTag.add(versionNumber);
		            String commitId = ref.getObjectId().getName();
		            commitsIdToAnalyse.add(commitId);
		            
		            System.out.println("TAG " + versionNumber);
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		
	}	
	public static void setInfos(){
		BufferedReader reader = setReader();
		
		int numberOfProjects = 0;
		
		try {
			numberOfProjects = Integer.parseInt(reader.readLine());
			versionsToAnalyseOfEachProject = new String[numberOfProjects];
			pathsToResults = new String[numberOfProjects];
			parameters = new String[numberOfProjects];
			
			for(int i = 0 ; i < numberOfProjects ; i++){
				String projectString[] = reader.readLine().split(" ");
				projectPaths.add(projectString[0]);
				versionsToAnalyseOfEachProject[i] = projectString[1];
				pathsToResults[i] = projectString[2];
				branchToAnalyze = projectString[4]; //last argument
				try{
					parameters[i] = projectString[3];
				}
				catch(Exception e){
					System.out.println("No arguments on project number " + i);
				}
			}
			
		reader.close();	
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public static BufferedReader setReader(){
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(PATH + "/info.txt"));
			return reader;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public static void downloadProject(String URL){
		
		new File(downloadPath).mkdirs();
		
		System.out.println("Downloading project");
		
		try{
			git = Git.cloneRepository().setURI(URL)
					.setDirectory(new File(downloadPath)).call();
			System.out.println("Donwnload done!");
			

		} catch (InvalidRemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransportException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GitAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void filesInAnalysis(String sourceFolder) throws IOException{
		File[] files = new File(sourceFolder).listFiles();
        for(File file: files){
            if(file.isDirectory()){
                filesInAnalysis(file.getAbsolutePath());
            } else {
            	// || file.getName().trim().endsWith(".h")
                if(file.getName().trim().endsWith(".c")){
                	copyFileUsingChannel(file, (new File(downloadPath + "\\analysis" + "\\" + file.getName())));
//                	FileUtils.copyFile(file, (new File(downloadPath + "\\analysis" + "\\" + file.getName())));
//                	Files.copy(file.toPath(), (new File(downloadPath + "\\analysis" + "\\" + file.getName())).toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
            }
        }
	}
	
	@SuppressWarnings("resource")
	public static void copyFileUsingChannel(File source, File dest) throws IOException {
	    FileChannel sourceChannel = null;
	    FileChannel destChannel = null;
	    try {
	        sourceChannel = new FileInputStream(source).getChannel();
	        destChannel = new FileOutputStream(dest).getChannel();
	        destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
	       }finally{
	           sourceChannel.close();
	           destChannel.close();
	       }
	}
	
	public static void changeToVersion(String commitId){
		System.out.println("Changing version...");
		try {
			git.checkout().setCreateBranch( true ).setName(commitId).setStartPoint( commitId ).call();
		} catch (GitAPIException e) {
			try {
				git.checkout().setCreateBranch( false ).setName(commitId).call();
			} catch (GitAPIException e1) {
				e1.printStackTrace();
			}
			//do nothing
		}
		System.out.println("Changed");
	}
	
	public static List<String> settingUp(String commitId) throws IOException{
		List<String> filesModified = null;

		if(!lastCommitAnalysed.equals("")){
			List<String> filesModifiedRightPath = new ArrayList<String>();
			//get the difference between the last commit and the new
			try {
				filesModified = diffFilesInCommits(lastCommitAnalysed, commitId);
				
				//adding only the new files to the analysis directory
				for(String fileModified : filesModified){
					String file = new File(fileModified).getName();
					filesModifiedRightPath.add(file);
				}
				
			} catch (GitAPIException e) {
				e.printStackTrace();
			}
			lastCommitAnalysed = commitId;
			filesInAnalysis(downloadPath);
			return filesModifiedRightPath;
		}
		new File(downloadPath + "\\analysis").mkdir();
		filesInAnalysis(downloadPath);
		
		lastCommitAnalysed = commitId;
		return filesModified;
	}
	
	public static int tryToCopy(String fileModified, String file){
		try {
			Files.copy(new File(downloadPath +""+ fileModified).toPath(), new File(downloadPath + "analysis\\" + file).toPath(),StandardCopyOption.REPLACE_EXISTING);
			return 1;
		} catch (IOException e) {
			// do nothing
			e.printStackTrace();
			return 0;
		}
	}
	
	//this method will only return the .c files modified
	public static List<String> diffFilesInCommits(String oldCommitId, String newCommitId) throws GitAPIException, IOException{
		List<DiffEntry> diffs = git.diff()
                .setOldTree(prepareTreeParser(git.getRepository(), oldCommitId))
                .setNewTree(prepareTreeParser(git.getRepository(), newCommitId))
                .call();

        System.out.println("Found: " + diffs.size() + " differences");
        List<String> modifiedFiles = new ArrayList<String>(diffs.size());
      	filesToDeleteFromAnalysisFolder = new ArrayList<String>(diffs.size());
        for (DiffEntry diff : diffs) {
        	noChangesInCFiles = true;
    		if(diff.getChangeType().toString().equals("DELETE")){
        		if(diff.getOldPath().endsWith(".c")){
        			noChangesInCFiles = false;
        			System.out.println(new File(diff.getOldPath()).getName());
            		filesToDeleteFromAnalysisFolder.add(new File(diff.getOldPath()).getName());
            	}
        	}
        		
            //checking for .c file
    		else if(diff.getNewPath().endsWith(".c")){
    			noChangesInCFiles = false;
    			//this method will only get the name of file
            	modifiedFiles.add(diff.getNewPath());
            }
        }

        for(String fileToDelete : filesToDeleteFromAnalysisFolder){
        	try{
        		Files.delete(new File(downloadPath + "/analysis/" + fileToDelete).toPath());
        	}
        	catch(Exception e){
        		//do nothing
        	}
        	
        }
        
        return modifiedFiles;
	}
	
	//this method receives a path and return the .c file if exists
	public static String getFileFromPath(String path){
		String fileToReturn = "";
		for(int length = path.length()-1 ; length >= 0 ; length--){
			if(path.charAt(length) != '/' && path.charAt(length) != '\\'){
				fileToReturn = fileToReturn + path.charAt(length);
			}
			else{
				break;
			}
		}
		//reverse the string
		return new StringBuilder(fileToReturn).reverse().toString();
		
	}
	
	private static AbstractTreeIterator prepareTreeParser(Repository repository, String objectId) throws IOException {
        // from the commit we can build the tree which allows us to construct the TreeParser
        //noinspection Duplicates
    	
        try (RevWalk walk = new RevWalk(repository)) {
            RevCommit commit = walk.parseCommit(repository.resolve(objectId));
            RevTree tree = walk.parseTree(commit.getTree().getId());
            PersonIdent authorIdent = commit.getAuthorIdent();
    		commitDate = authorIdent.getWhen();
    		System.out.println("Commit DATE "+commitDate);

            CanonicalTreeParser treeParser = new CanonicalTreeParser();
            try (ObjectReader reader = repository.newObjectReader()) {
                treeParser.reset(reader, tree.getId());
            }

            walk.dispose();

            return treeParser;
        }
    }
	
	public static boolean[] settingsFromParameters(){
		
		boolean settings[] = {true,true,false,false,false,true};
		//defining arguments
		for(String arguments : parameters){
			if(arguments == null){
				System.out.println("Specify -v or -allC");
				return settings;
			}
				
			for(String argument : arguments.split("-")){
				if(argument.contains("nodl")){
					settings[0] = false;
				}
				else if(argument.contains("allC")){
					settings[1] = false;
					settings[2] = true;
				}
				else if(argument.contains("stubs")){
					settings[3] = true;
				}
				else if(argument.contains("allR")){
					settings[1] = false;
					settings[4] = true;
				}
				else if(argument.contains("nocommit")){
					settings[5] = false;
				}
			}
		}
		return settings;
	}
	
	public static int getIndexOfPastAnalysis(){
		File[] allAnalysis = new File(downloadPath + "results/csv/Variabilities").listFiles();
		
		if(allAnalysis == null){
			return -1;
		}
		else{
			return allAnalysis.length;
		}
		
	}
	
	private static void deleteAllFromAnalysisFolder() {
		try {
			FileUtils.cleanDirectory(new File(downloadPath + "analysis"));
		} catch (IOException e) {
			System.out.println("Analysis folder not found to delete!");
		}
	}
	
}
