package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import analysis.core.Project;
import analysis.core.ResultsLogger;
import cdt.handlers.SampleHandler;

public class Starter {

	String downloadPath = "";
	boolean noStubs = false;
	Project project;
	
	//just to register
	public static int numberOfCFiles = 0;
	
	public Starter(String downloadPath, boolean noStubs){
		this.noStubs = noStubs;
		this.downloadPath = downloadPath;
		//defining arrays for the Metrics
		
		//creating platform.h and stubs.h
		try {
			new File(downloadPath + "platform.h").createNewFile();
			new File(downloadPath + "include").mkdirs();
			new File(downloadPath + "include/stubs.h").createNewFile();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		File platform = new File(downloadPath + "platform.h");
		if (platform.length() == 0) {
			project = new Project(downloadPath + "analysis", downloadPath + "include\\stubs.h");
		} else {
			project = new Project(downloadPath + "analysis", downloadPath + "platform.h");
		}
		project.setName(main.Main.currentProject);
	}
	
	public String[] defineFiles(String[] filesToAnalyze){
		File path = new File(downloadPath + "analysis");
		File[] files = path.listFiles();
		numberOfCFiles = files.length;
		List<String> allFilesInAnalysisFolder = new ArrayList<String>(numberOfCFiles);
		
		ResultsLogger.write("	number of .c files: " + numberOfCFiles);
		if(filesToAnalyze == null){
			for(File file : files){
				allFilesInAnalysisFolder.add(file.getAbsolutePath());
			}
		}
		else{
			for(String file : filesToAnalyze){
				File fileWithCompletePath = new File(downloadPath + "analysis/" +file);
				allFilesInAnalysisFolder.add(fileWithCompletePath.getAbsolutePath());
			}
		}
		
		filesToAnalyze = new String[allFilesInAnalysisFolder.size()];
		filesToAnalyze = allFilesInAnalysisFolder.toArray(filesToAnalyze);
		return filesToAnalyze;
	}
	
	public Project start(String filesToAnalyze[]){
		
		String[] files = defineFiles(filesToAnalyze);
		
		//creating include/stubs.h and platform.h
		if(!noStubs){
			createStubs(files);
		}
		
		//start the analysis
		return startAnalyser(files);
	}
	
	public Project startAnalyser(String filesToAnalyze[]){

		//start the analyzer
		project.analyze(filesToAnalyze);
		return project;
	}
	
	public void createStubs(String[] files){
		//creating CProject to create include/stubs.h and platform.h
		SampleHandler.createCProject(Main.currentProject);
		
		try {
			//creating platform.h and include/stubs.h
			SampleHandler.analyzeFilesInSrc(files);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
	}
	
}
