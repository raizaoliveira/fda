package finegrained;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import analysis.Dependency;
import analysis.ID;
import analysis.Link;
import analysis.Variability;
import analysis.core.Function;
import analysis.core.ProgramElement;
import analysis.core.Variable;
import main.Main;
import metrics.Metrics;

public class Reports extends Metrics {
	public static int rindex = 1;
	public static Set<Dependency> preservedDependencies = new HashSet<Dependency>(500);
	public static Set<Dependency> changedDependencies = new HashSet<Dependency>(500);
	public static Set<Variability> variabilityDependencyComposer = new HashSet<Variability>(allVariabilities);
	private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	
	

	
	public static void Dependencies(){
		
		if(!Main.analyseThisTime){
			//adding the current dps to previous dps
			previousDependenciesList = new HashSet<Dependency>(allDependencies.size());
			previousVariabilitiesList = new HashSet<Variability>(allVariabilities.size());
			variabilityDependencyComposer = new HashSet<Variability>(allVariabilities);
			
			for(Variability currentVar : allVariabilities) {
				previousVariabilitiesList.add(currentVar);
			}
			for(Dependency currentDp : allDependencies){
				previousDependenciesList.add(currentDp);
			}
			rindex = Main.getIndexOfPastAnalysis() + 1;
			Main.analyseThisTime = true;
			return;
		}
		
		try {
			PrintWriter printCoarse = null;
			PrintWriter printFine = null;
			PrintWriter printImpact = null;
			PrintWriter printVar = null;
			FileWriter arqvar = null; FileWriter coarsearq = null; FileWriter fineFile = null; FileWriter impactFile = null;
			new File(Main.PATH + "\\" + Main.currentProject + "\\results\\csv\\PermanentsDependencies").mkdirs();
			new File(Main.PATH + "\\" + Main.currentProject + "\\results\\csv\\Variabilities").mkdirs();
			if(Main.currentTag.size() == 0){
				impactFile = new FileWriter(Main.PATH + "\\" + Main.currentProject + "\\results\\csv\\PermanentsDependencies\\"+"_Impact_"+ ".csv",true);
				fineFile = new FileWriter(Main.PATH + "\\" + Main.currentProject + "\\results\\csv\\PermanentsDependencies\\"+"_FineGrained_"+ ".csv",true);
				coarsearq = new FileWriter(Main.PATH + "\\" + Main.currentProject + "\\results\\csv\\PermanentsDependencies\\"+"_CoarseGrained_"+ ".csv",true);
				arqvar = new FileWriter(Main.PATH + "\\" + Main.currentProject + "\\results\\csv\\Variabilities\\"+"variabilities" + ".csv",true);
			}else {
				impactFile = new FileWriter(Main.PATH + "\\" + Main.currentProject + "\\results\\csv\\PermanentsDependencies\\"+"_Impact_" + ".csv",true);
				fineFile = new FileWriter(Main.PATH + "\\" + Main.currentProject + "\\results\\csv\\PermanentsDependencies\\"+"_FineGrained_" + ".csv",true);
				coarsearq = new FileWriter(Main.PATH + "\\" + Main.currentProject + "\\results\\csv\\PermanentsDependencies\\"+"_CoarseGrained_"+ ".csv",true);
				arqvar = new FileWriter(Main.PATH + "\\" + Main.currentProject + "\\results\\csv\\Variabilities\\"+"variabilities"+ ".csv",true);
			}
			printCoarse = new PrintWriter(coarsearq);
			printFine = new PrintWriter(fineFile);
			printImpact = new PrintWriter(impactFile);
			printVar = new PrintWriter(arqvar);
			
			FoundImpact foundImpact = new FoundImpact();
			FoundChanges atest = new FoundChanges();
			
			if(rindex <= 1){
				//Primeira vez todas as dependencias s�o novas
				//printNewDependencies(allDependencies,gravarArqn);
				printCoarse.println("Date, Evolution, Variabilities, TotalDependencies, DependenciesDeleted, DependenciesAdditions, DependenciesPreserved, DependenciesChanged");
				printCoarse.println("--"+","+rindex+","+ allVariabilities.size()+","+allDependencies.size()+","+getAllDeadDependencies().size()+","+getAllNewDependencies().size()+","+preservedDependencies.size()+","+changedDependencies.size());
			
				printFine.println("Date, Evolution,Number of Functions Change,Number of Variables Change,Function(+),Variable(+),Function(-),Variable(-),Modifier Change(Var.),Especifier Change(Var.),Qualifier Change(Var.),Type Change(Var.),Function Return Change,	Modifier Change (Func.), Specifier Change (Func.),Qualifier Change (Func.),Parameters Change (Func.)");
				printVariabilities(printVar);
			} 
			
			//Found Preserved Dependencies
			if(rindex > 1){//this analysis occurred once
				
				System.out.println(DATE_FORMAT.format(Main.commitDate));
				foundPreservedDependencies(atest, foundImpact);
				printVariabilities(printVar);
				reportCoarseGrained(printCoarse);
				reportFineGrained(printFine, atest);
				//aaaaa(foundImpact);
				//reportPointImpact(printImpact, foundImpact);
				
			}
			
			variabilityDependencyComposer = new HashSet<Variability>(allVariabilities);
			previousVariabilitiesList = new HashSet<Variability>(allVariabilities.size());
			for(Variability currentVar : allVariabilities) {
				previousVariabilitiesList.add(currentVar);
			}

			//adding the current dps to previous dps
			previousDependenciesList =  new HashSet<Dependency>(allDependencies.size());
			for(Dependency currentDp : allDependencies){
				previousDependenciesList.add(currentDp);
			}
			
			changedDependencies = new HashSet<Dependency>(500);
			rindex++;
			
			fineFile.close();
			printFine.close();
			printCoarse.close();
			coarsearq.close();
			impactFile.close();
			printImpact.close();
			arqvar.close();
			printVar.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	public static void foundPreservedDependencies(FoundChanges atest, FoundImpact foundImpact) {
		
		for(Dependency dpCurrentVersion : allDependencies){
			List<Link> newLinks = new ArrayList<Link>(dpCurrentVersion.getLinks().size());
			List<Link> deadLinks = new ArrayList<Link>(dpCurrentVersion.getLinks().size());
			List<Link> permanentLinks = new ArrayList<Link>(dpCurrentVersion.getLinks().size());
			boolean isPreserved = false;
			
			if(!variabilityDependencyComposer.contains(dpCurrentVersion.getVariabilityA())) {
				variabilityDependencyComposer.add(dpCurrentVersion.getVariabilityA());
			}
			if(!variabilityDependencyComposer.contains(dpCurrentVersion.getVariabilityB())) {
				variabilityDependencyComposer.add(dpCurrentVersion.getVariabilityB());
			}
			
			for(Dependency dpPrevious : previousDependenciesList){
				boolean sameDependency = isSameDependency(dpPrevious, dpCurrentVersion);
				if(sameDependency){
					if (!containsDep(preservedDependencies, dpCurrentVersion)) {
						preservedDependencies.add(dpCurrentVersion);
							
					}
					
					//Looking for new links
					for(Link linkFromCurrentDp : dpCurrentVersion.getLinks()){
						for(Link linkFromPreviousDp : dpPrevious.getLinks()){
							if(isSameLink(linkFromPreviousDp, linkFromCurrentDp)){
								isPreserved = true;
								break;
							}
						}
						if(!isPreserved) {
							newLinks.add(linkFromCurrentDp);
							if (!containsDep(changedDependencies, dpCurrentVersion)) {
								changedDependencies.add(dpCurrentVersion);
								System.out.println("***************LINK ADD ***************************");
								atest.addLink(linkFromCurrentDp);
								foundImpact.addImpactProgramElement(linkFromCurrentDp.getCaller());
								foundImpact.addImpactVariability(dpCurrentVersion.getVariabilityB());
							}
						}
						isPreserved = false;
					}
					
					//Looking for dead links
					for(Link linkFromPreviousDp : dpPrevious.getLinks()){
						for(Link linkFromCurrentDp : dpCurrentVersion.getLinks()){
							if(isSameLink(linkFromPreviousDp, linkFromCurrentDp)){
								isPreserved = true;
								break;
							}
						}
						if(!isPreserved) {
							deadLinks.add(linkFromPreviousDp);
							if (!containsDep(changedDependencies, dpCurrentVersion)) {
								changedDependencies.add(dpCurrentVersion);
								System.out.println("*************** LINK EXCLUIDO ***************************");
								atest.delLink(linkFromPreviousDp);
								foundImpact.addImpactProgramElement(linkFromPreviousDp.getCaller());
								foundImpact.addImpactVariability(dpCurrentVersion.getVariabilityB());
							}
						}
						isPreserved = false;
					}
						
					//Looking for preserved links
					for(Link linkFromCurrentDp : dpCurrentVersion.getLinks()){
						if(!newLinks.contains(linkFromCurrentDp)) {
							if(!permanentLinks.contains(linkFromCurrentDp)) {
								permanentLinks.add(linkFromCurrentDp);
							}
						}
					}
					
					//Looking for fine grained changes in preserved links
					for(Link linkFromPreviousDp: dpPrevious.getLinks()) {
						for(Link preservedLink: permanentLinks) {
							if (isSameLink(linkFromPreviousDp, preservedLink)) {
								if(atest.differ(linkFromPreviousDp, preservedLink)) {
									if (!containsDep(changedDependencies, dpCurrentVersion)) {
										changedDependencies.add(dpCurrentVersion);
										System.out.println("***************MUDANCA GRAINED FINE ***************************");
										foundImpact.addImpactProgramElement(preservedLink.getCaller());
										foundImpact.addImpactVariability(dpCurrentVersion.getVariabilityB());
									}
								}	
							}	
						}
					}
					
					
					/*******************************
					for (ProgramElement peold: dpPrevious.getFuncDeclarated()) {
						for (ProgramElement patual:dpCurrentVersion.getFuncDeclarated()) {
							if(patual.getName().compareTo(peold.getName()) == 0) {
								System.out.println("old func:  "+peold.getName()+"  equal  "+patual.getName());
								if(patual.getModifier() != peold.getModifier())
									System.out.println(patual.getName()+" change modifier  "+patual.getModifier());
								
								if(patual.getQualifier() != peold.getQualifier())
									System.out.println(patual.getName()+" change Qualifier  "+patual.getQualifier());
								
								if(patual.getSpecifier() != peold.getSpecifier())
									System.out.println(patual.getName()+" change specifier  "+patual.getSpecifier());
								
								if(patual.getType() != peold.getType())
									System.out.println(patual.getName()+" change Type  "+patual.getType());
								
								if (patual.id == ID.Function) {
									if (((Function)patual).getParameters().size() != ((Function)peold).getParameters().size()) {
										System.out.println(patual.getName()+" change parameter  ");
									}
								}
								
							}
							
						}
					}
					
					
				*******************************/
					
					
					
				}
			}
		}
	}
	
	
	public static void aaaaa(FoundImpact foundImpact) {
		System.out.println("FOUND IMPACT AAAAAAAAAAAAAAAAAA   "+foundImpact.getImpactVariability().size());
		for (Variability var: foundImpact.getImpactVariability()) {
			System.out.println("AAAAAAAAAAAAAAAAA   "+var.getName());
			if(foundImpact.compareVariabilities(var, previousVariabilitiesList)) {
				System.out.println("IMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTA");
				foundImpact.addImpactPoints();
			}
			
			for (Function func: var.getDeclaredFunctions()) {
				if(foundImpact.compareProgramElement((ProgramElement)func, var, previousVariabilitiesList)) {
					System.out.println("IMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTA");
					foundImpact.addImpactFunctions();
				}
				
			}
			
			for (Variable variable: var.getDeclaredGlobalVariables()) {
				if(foundImpact.compareProgramElement((ProgramElement)variable, var, previousVariabilitiesList)) {
					System.out.println("IMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTAIMPACTA");
					foundImpact.addImpactVariables();
				}
				
			}

		}
		
	}

	public static void printVariabilities(PrintWriter gravarArq) {
		int nfunction = 0;
		int nvariable = 0;
		
		for (Dependency dep: allDependencies) {
			
			System.out.println("Dependencia: "+dep.getVariabilityA().getName()+" <> "+dep.getVariabilityB().getName()+"  Funcs: "+dep.getNumberOfFuncDeclarated()+" Var: "+dep.getNumberOfVarDeclarated());
			for (ProgramElement f: dep.getFuncDeclarated()) {
				System.out.println("Funcs: "+f.getName());
			}
			
			nfunction += dep.getNumberOfFuncDeclarated();
			nvariable += dep.getNumberOfVarDeclarated();
		}
		
		gravarArq.println(rindex+","+allVariabilities.size()+","+allDependencies.size()+","+nfunction+","+nvariable);
		nfunction = 0;
		nvariable = 0;
	}
	
	public static void reportCoarseGrained(PrintWriter gravarArq) {
		//Evolu��o	Variabilities	DependenciesTotal	DependenciesExcluded	DependenciesNew	  Dependenciespreserved		DependenciesChanged
		//csvcaller ="Evolution, Variabilities, TotalDependencies, DependenciesDeleted, DependenciesAdditions, DependenciesPreserved, DependenciesChanged";
	
		gravarArq.println(DATE_FORMAT.format(Main.commitDate)+","+rindex+","+allVariabilities.size()+","+allDependencies.size()+","+getAllDeadDependencies().size()+","+getAllNewDependencies().size()+","+preservedDependencies.size()+","+changedDependencies.size());
	}
	
	public static void reportFineGrained(PrintWriter printFile, FoundChanges atest) {
		
		printFile.println(DATE_FORMAT.format(Main.commitDate)+","+rindex+","+atest.funch.get_total()+","+atest.varch.get_total()+","+atest.funch.get_Insert()+","+atest.varch.get_Insert()+","+atest.funch.get_Removal()+","+atest.varch.get_Removal()+","+atest.varch.modifierchanges.get_total()+","+atest.varch.specifierchanges.get_total()+","+atest.varch.qualifierchanges.get_total()+","+atest.varch.typeschanges.get_total()+","+atest.funch.typeschanges.get_total()+","+atest.funch.modifierchanges.get_total()+","+atest.funch.specifierchanges.get_total()+","+atest.funch.qualifierchanges.get_total()+","+atest.cont_change_parameter);
		
	}
	
	public static void reportPointImpact(PrintWriter printFile, FoundImpact foundImpact) {
		printFile.println(DATE_FORMAT.format(Main.commitDate)+","+rindex+","+changedDependencies.size()+","+foundImpact.getImpactVariability().size()+","+foundImpact.getImpactProgramElement().size()+","+foundImpact.getImpactPoints()+","+foundImpact.getImpactFunctions()+","+foundImpact.getImpactVariables());
	}
		
	public static boolean isSameDependency(Dependency dep1, Dependency dep2) {
		return (dep1.getVariabilityA().getName().equals(dep2.getVariabilityA().getName()) && 
				dep1.getVariabilityB().getName().equals(dep2.getVariabilityB().getName()));			
	}
	
	public static boolean isSameLink(Link lk1, Link lk2) {
		return (lk1.callee.getName().equals(lk2.callee.getName()) && 
				lk1.caller.getName().equals(lk2.caller.getName()));
	}
	
	public static boolean containsDep(Set<Dependency> Dependencies, Dependency dependency) {
		for (Dependency dp: Dependencies) {
				if(isSameDependency(dp, dependency)) {
					return true;
				}
		}
		return false;
		
	}
	

}
