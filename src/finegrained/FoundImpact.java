package finegrained;

import java.util.HashSet;
import java.util.Set;

import analysis.Dependency;
import analysis.ID;
import analysis.Link;
import analysis.Variability;
import analysis.core.Function;
import analysis.core.ProgramElement;
import analysis.core.Variable;
import metrics.Metrics;
import tree.Id;
import tree.Opt;

public class FoundImpact extends FoundChanges {
	
	private Set<Variability> impactSetVariability = new HashSet<Variability>(1000);
	private Set<ProgramElement> impactSetProgramElement = new HashSet<ProgramElement>(1000);
	private Set<String> costumervariabilities = new HashSet<>(Metrics.numberOfVariabilities);
	private int impactPoints; 
	private int impactFunctions;
	private int impactVariables;
	
	private Set<ProgramElement> elementsAnalysed = new HashSet<ProgramElement>(1000);
	
	FoundImpact(){
		this.impactPoints = 0;
		this.impactFunctions = 0;
		this.impactVariables = 0;
	}
	

	
	public void addImpactVariability(Variability variability) {
		this.impactSetVariability.add(variability);
	}
	
	public void addImpactProgramElement(ProgramElement pe) {
		this.impactSetProgramElement.add(pe);
	}
	
	
	public Set<Variability> getImpactVariability() {
		return impactSetVariability;
	}
	
	public Set<ProgramElement> getImpactProgramElement() {
		return impactSetProgramElement;
	}
	
	public int getImpactPoints() {
		return impactPoints;
	}
	
	public void addImpactPoints() {
		this.impactPoints +=1;
	}
	

	
	
	public void addNewCostumer(ProgramElement element) {
		System.out.println("call the new costumer");
		String costvar="";
		if(element.id == ID.Function) {
			Function f = ((Function)element);
			for(Opt opt : f.getDirectives()){
				costvar = opt.getPresenceCondition().toString();
			}

		}else {
			Variable v = ((Variable)element);
			costvar = v.getPresenceCondition().toString();
		}
		if(!costumervariabilities.contains(costvar)) {
			costumervariabilities.add(costvar);
		}
		
	}
	
	public Boolean compareVariabilities(Variability var, Set<Variability> variabilitiesPrevious ) {
		System.out.println("comparando variabilidades   "+var.getName()+"   PREVIOUS     "+variabilitiesPrevious.size());
		for(Variability varPrevious: variabilitiesPrevious) {
			if(varPrevious.getName().equals(var.getName())) {
				System.out.println("VARIABILIDADES IGUAIS");
				return isVariabilityChanged(varPrevious, var);
			}
			
		}
		return false;
	}
	
	
	public Boolean compareProgramElement(ProgramElement pe, Variability var, Set<Variability> variabilitiesPrevious) {
		Boolean impact = false;
		for(Variability varPrevious: variabilitiesPrevious) {
			if(varPrevious.getName().equals(var.getName())) {
				System.out.println("VARIABILIDADES IGUAIS");
				
				if (!verifiedElement(pe)) {
					System.out.println("ENTROU ENTROU ENTROU ENTROU ENTROU ENTROU ENTROU ENTROU ENTROU ENTROU ENTROU    ");
					if(pe.id == ID.Variable) {
						impact = verifyChangeinVariable((Variable)pe, varPrevious);
						System.out.println("VARIAVELLL >>>>   "+pe.getName());
						if (impact)
							System.out.println("VARIAVEL IMPACTADAAAAAAAAAAAAAAAAAA VARIAVEL IMPACTADAAAAAAAAAAAAAAAAAA VARIAVEL IMPACTADAAAAAAAAAAAAAAAAAA VARIAVEL IMPACTADAAAAAAAAAAAAAAAAAA");
					}	
					
					
					if(pe.id == ID.Function) {
						System.out.println("FUNCAAOOOOOOOOO >>>>   "+pe.getName());
						impact = verifyChangeinFunction((Function)pe, varPrevious);
						if (impact)
							System.out.println("VARIAVEL IMPACTADAAAAAAAAAAAAAAAAAA VARIAVEL IMPACTADAAAAAAAAAAAAAAAAAA VARIAVEL IMPACTADAAAAAAAAAAAAAAAAAA VARIAVEL IMPACTADAAAAAAAAAAAAAAAAAA");
					}
				}
				
			}
			
		}
		return impact;
	} 
	
	
	public Boolean isVariabilityChanged(Variability variabilityPrevious, Variability variabilityCurrent) {
		/*Verificar diff em quantidade de elementos de programa
		 *Fun��es inseridas
		 * Fun��es removidas
		 * Variaveis inseridas
		 * Variaveis removidas
		 * Fun��es alteradas
		 * Variaveis alteradas
		 * */
		Boolean impact = false;
		if(variabilityPrevious.getDeclaredFunctions().size() != variabilityCurrent.getDeclaredFunctions().size() ) {
			impact = true;
			
		}
		
		if(variabilityPrevious.getNumberOfProgramElements() != variabilityCurrent.getNumberOfProgramElements() ) {
			impact = true;
			
		}
		
		if(variabilityPrevious.getUsedFunctions().size() != variabilityCurrent.getUsedFunctions().size() ) {
			impact = true;
			
		}
		
		if(variabilityPrevious.getNumberOfLocalVariables() != variabilityCurrent.getNumberOfLocalVariables() ) {
			impact = true;
			
		}
		
		System.out.println("******************  variabilityCurrent has   "+variabilityCurrent.getDeclaredFunctions().size()+"  Declared Functions");
		for(Function func: variabilityCurrent.getDeclaredFunctions()) {
			if( verifyChangeinFunction(func,variabilityPrevious))
				impact = true;
		}
		
		for(Variable var: variabilityCurrent.getDeclaredGlobalVariables()) {
			if( verifyChangeinVariable(var,variabilityPrevious))
				impact = true;
		}
		
		return impact;
	
	}


	private Boolean verifyChangeinFunction(Function function,Variability variabilityPrevious) {
		Boolean impact = false;

		for(Function func: variabilityPrevious.getDeclaredFunctions()) {
			if(func.getName().equals(function.getName())) {
				System.out.println("FUNCAOOOOOOOOOOOOOOOOOOOOOOOOOOO   "+function.getName());
				if(function.getType() != func.getType())
					return true;
				if(function.getModifier() != func.getModifier())
					return true;
				if(function.getQualifier() != func.getQualifier())
					return true;
				if(function.getSpecifier() != func.getSpecifier())
					return true;
				if(function.getParameters().size() != func.getParameters().size())
					return true;
				
				if(function.getLocalVariables().size() != func.getLocalVariables().size())
					return true;
				
				for(Id localVariable: function.getLocalVariables()) {
					for(Id localVarPrevious: func.getLocalVariables()) {
						if(localVariable.getType() != localVarPrevious.getType())
							return true;
						if(localVariable.getModifier() != localVarPrevious.getModifier())
							return true;
						if(localVariable.getQualifier() != localVarPrevious.getQualifier())
							return true;
						if(localVariable.getSpecifier() != localVarPrevious.getSpecifier())
							return true;
					}
				} 
				
				
			}
		}
		
		
		
		return impact;
		
	}


	private Boolean verifyChangeinVariable(Variable variable,Variability previousVariability) {
		Boolean impact = false;
		
		for(Variable var: previousVariability.getDeclaredGlobalVariables()) {
			if(var.getName().equals(variable.getName())) {
				System.out.println("VARIAVELLLLLLLLLLLLLL   "+variable.getName());
				if(variable.getType() != var.getType())
					return true;
				if(variable.getModifier() != var.getModifier())
					return true;
				if(variable.getQualifier() != var.getQualifier())
					return true;
				if(variable.getSpecifier() != var.getSpecifier())
					return true;
				
			}
		}
		
		
		
		
		return impact;
		
	}



	public int getImpactFunctions() {
		return impactFunctions;
	}


	public void addImpactFunctions() {
		this.impactFunctions++;
	}

	public int getImpactVariables() {
		return impactVariables;
	}

	public void addImpactVariables() {
		this.impactVariables++;
	}
	
	public Boolean verifiedElement(ProgramElement pe) {
		
		if (this.elementsAnalysed.contains(pe)) {
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Ja existe "+pe.getName() );
			
			return true;
			
		}
			
		
		if (!this.elementsAnalysed.contains(pe)) {
			this.elementsAnalysed.add(pe);
			System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Adicionado "+pe.getName() );
			return false;
		}
		
		
		return null;
	}


}
